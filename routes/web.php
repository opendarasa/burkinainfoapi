<?php
use GuzzleHttp\Client;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/redirect','ApiController@grantAccessCode');
Route::resource('users','UserController');
Route::get('/callback','ApiController@callback');

Route::get('testapi',function(){
	
  $token = config('api.access_token');

     $client = new Client(['base_uri' => url('/').'/api/']);
     $headers = [
    'Authorization' =>$token,        
    'Accept'        => 'application/json',
     ];

     $response = $client->request('GET', 'user', [
        'headers' => $headers
    ]);

      $newuser=json_decode($response->getBody());

      return $newuser;
});

Route::get('getToken','ApiController@generateToken')->middleware('auth');
//Route::get('programs','ProgramController@index')->middleware('auth');
//Route::get('programs/{slug}','ProgramController@edit')->middleware('auth');
Route::resource('programs','ProgramController');