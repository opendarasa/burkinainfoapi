<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
class EmissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emissions')->insert([
            'title' => Str::random(10),
            'video_url' => 'https://www.youtube.com/watch?v=F-3rkU_PhHk',
            'date'=>Carbon::now(),
            'slug'=>Str::random(30),
            'image'=>'emission.png',
            'content'=>Str::random(100),
            'user_id'=>1,
        ]);
    }
}
