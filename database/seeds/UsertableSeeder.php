<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
class UsertableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Burkina Info',
            'email' => 'admin@burkinainfo.com',
            'profile_pic'=>'avatar.png',
            'username'=>uniqid(),
            'roleid'=>1,
            'password' => bcrypt('secret'),
        ]);
         DB::table('users')->insert([
            'name' => 'Ismaël OUEDRAOGO',
            'email' => 'ismael@burkinainfo.com',
            'profile_pic'=>'avatar.png',
            'username'=>uniqid(),
            'roleid'=>1,
            'password' => bcrypt('burkinainfoApp'),
        ]);
    }
}
