<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
class ProgramTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $program=array();
        $programs=trans('app.emissions');
        
        if(count($programs)>0)
        {
            foreach ($programs as $program) {
                
                    
            

            DB::table('programs')->insert([
            'title' => $program['title'],
            'content' =>$program['content'],
            'video_url' => $program['video_url'],
            'date'=>$program['date'],
            'slug'=>$program['slug'],
            'image'=>$program['image'],
            'user_id'=>$program['user_id'],
            ]);
           }

        }
        
        
    }
}
