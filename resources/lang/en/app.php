<?php

use Carbon\Carbon;

return [
'token'=>'$2y$10$1Zh1sXmnHUpkv7Ta7DuVyeIkHR05ZZtmrC6gtQ8i7ug3vdvoBcyF6',

'emissions'=>[
	[
		'title'=>'Périscope',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 52 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Jeudi à 20h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Samedi à 14h30
					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now(),
		'slug'=>str_slug('periscope','-'),
		'user_id'=>1,
	],
	[
		'title'=>'Débat sport du weekend',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 52 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: PAD/Direct Dimanche à 11h00
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Dimanche:  22h15
					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(2),
		'slug'=>str_slug('Débat sport du weekend','-'),
		'user_id'=>1,

	],
	[
		'title'=>'Quartier des arts',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 26 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: PAD/Direct Mardi à 19h30 Jeudi à 19h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Vendredi : 12h30
					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(3),
		'slug'=>str_slug('Quartier des arts','-'),
		'user_id'=>1,
	],
	[
		'title'=>'Le Débat',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 26 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Mardi à 20h30 Vendredi à 20h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Vendredi : 12h30
					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(4),
		'slug'=>str_slug('Le Débat','-'),
		'user_id'=>1,
	],
	[
		'title'=>'L’édito',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 13 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Tous les jours à 08h15
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Du lundi au dimanche

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(5),
		'slug'=>str_slug('L’édito','-'),
		'user_id'=>1,
	],
	[
		'title'=>'SPORT VISION',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 26 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Lundi 20h30 et Mercredi 21h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Mardi : 12h30

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(6),
		'slug'=>str_slug('SPORT VISION','-'),
		'user_id'=>1,
	],

	[
		'title'=>'Diaspora 226',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 26 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Lundi à 19h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Vendredi : 21h30

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(7),
		'slug'=>str_slug('Diaspora 226','-'),
		'user_id'=>1,
	],
	[
		'title'=>'Le débat parlementaire',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 26 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Mercredi à 20h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Vendredi: 19h30

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(8),
		'slug'=>str_slug('Le débat parlementaire','-'),
		'user_id'=>1,
	],
	[
		'title'=>'C’EST MA SANTE',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 45 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Mercredi à 14h00 et à 22h15
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Vendredi : 14h00

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(9),
		'slug'=>str_slug('C’EST MA SANTE','-'),
		'user_id'=>1,
	],
	[
		'title'=>'FASO KUNKO',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 52 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Jeudi à 14h00
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Mardi : 22h15

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(10),
		'slug'=>str_slug('FASO KUNKO','-'),
		'user_id'=>1,
	],
	[
		'title'=>'Zoe neef seen pa bee',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 52 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Samedi à 20h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: Jeudi : 22h15

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(11),
		'slug'=>str_slug('Zoe neef seen pa bee','-'),
		'user_id'=>1,
	],
	[
		'title'=>'Le Journal de Ismaël OUEDRAOGO',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 26 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: Le dimanche à 12h30 Et à 19h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: 
						Lundi : 12h30 <br>
						Jeudi : 21h30

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(12),
		'slug'=>str_slug('Le Journal de Ismaël OUEDRAOGO','-'),
		'user_id'=>2,
	],

	[
		'title'=>'Le Grand déballage',
		'content'=>'<html>
					<ul>
					<li>
						<b>Durée</b>: 52 mins
					</li>
					<li>
						<b>Jour et Heure de diffusion</b>: PAD Dimanche à 20h30
					</li>
					<li>
						<b>Jour et Heure de rediffusion</b>: 
						Lundi : 14het 22h15<br>
						Vendredi : 22h15 <br>
						Dimanche : 06h00

					</li>
					</ul>
					</html>',
		'image'=>'https://via.placeholder.com/150',
		'video_url'=>'',
		'date'=>Carbon::now()->addMinutes(12),
		'slug'=>str_slug('Le Grand déballage','-'),
		'user_id'=>2,
	],

	// users.idex

	

],

'role'=>[
		'1'=>'Administrateur',
		'2'=>'Redacteur',
	],
'status'=>[
	'0'=>'Désactivé',
	'1'=>'Actif',
],

];