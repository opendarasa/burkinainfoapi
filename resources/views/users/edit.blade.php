@extends('layouts.program-template')


@section('content')
  <div class="container">
        
          
        <p style="color:red">
        	<small><i>L'Étoile * signiefie champ obligatoir.</i></small>
        </p>
        
      <div class="col-sm-10 ">
      	@if(session()->has('status'))
        <div class="col-lg-9 alert alert-success text-center" style="margin-left:25%;">
          {{session('status')}}
        </div>
        @endif
        @if($errors->any())
        <div class="col-lg-9 alert alert-danger text-center" style="margin-left:25%;">
          {{$errors->first()}}
        </div>
        @endif
         {!! Form::model($user, [
    'method' => 'PATCH',
    'route' => ['users.update', $user->username],
    'files'=>'true',
    'id'=>'edit_user_form'

    ]) !!}

    {{csrf_field()}}

    {{Form::hidden('password',$user->password)}}
    <div class="form-group row">
    {{Form::label('name','Nom Complet *',['class'=>'col-lg-3 col-form-label form-control-label'])}}

    <div class="col-lg-9">
    {{Form::text('name',null,['class'=>'form-control','id'=>'name'])}}
    <small style="color:red" id="name_alert"></small>
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('username','Pseudonyme *',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::text('username',$user->username,['class'=>'form-control','disabled'=>'false','id'=>'username'])}}
    <small style="color:red" id="username_alert"></small>
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('email','Email *',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
    {{Form::email('email',$user->email,['class'=>'form-control','id'=>'email'])}}
    <small style="color:red" id="email_alert"></small>
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('role','Role *',['class'=>'col-lg-3 col-form-label form-control-label '])}}
    <div class="col-lg-9">
    {{Form::select('roleid',[
    	'1'=>'Administrateur',
		'2'=>'Redacteur',
    ],$user->roleid,['id'=>'role','class'=>'form-control'])}}
    <small style="color:red" id="role_alert"></small>
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('status','Status du compte *',['class'=>'col-lg-3 col-form-label form-control-label '])}}
    <div class="col-lg-9">
    {{Form::select('status',[
    	'0'=>'Désactivé',
		'1'=>'Actif',
    ],$user->status,['id'=>'status','class'=>'form-control'])}}
    <small style="color:red" id="status_alert"></small>
    </div>
    </div>
    
    <div class="form-group row">
    {{Form::label('profile_pic','Photo de Profile',['class'=>'col-lg-3 col-form-label form-control-label '])}}
    <div class="col-lg-9">
    {{Form::file('profile_pic',['id'=>'profile_pic'])}}
    </div>
    </div>
    <div class="form-group row">
    <label class="col-lg-3 col-form-label form-control-label">

    </label>
    <div class="col-lg-9">
    <a href="{{route('users.index')}}" type="button" class="btn btn-secondary pull-left">
        Fermer
    </a>

    <input type="submit" class="btn btn-primary pull-right" value="Sauvegarder">
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    </div>
    
    {{Form::close()}}



      </div>
        
        
      </div>

@endsection