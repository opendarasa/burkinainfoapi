@extends('layouts.editor-template')


@section('content')
  <div class="container">
        
          
        <p style="color:red">
        	<small><i>L'Étoile * signiefie champ obligatoir.</i></small>
        </p>
        
      <div class="col-sm-9 ">
        @if(session()->has('status'))
        <div class="col-lg-9 alert alert-success text-center" style="margin-left:25%;">
          {{session('status')}}
        </div>
        @endif
        @if($errors->any())
        <div class="col-lg-9 alert alert-danger text-center" style="margin-left:25%;">
          {{$errors->first()}}
        </div>
        @endif
         {{Form::open( array(
             'route' => 'programs.store', 
             'id' => 'edit_program_form', 
             'class'=>'form',
             'files' => true))}}

    

    
    <div class="form-group row">
    {{Form::label('title','Titre du Programme *',['class'=>'col-lg-3 col-form-label form-control-label'])}}

    <div class="col-lg-9">
    {{Form::text('title',null,['class'=>'form-control','id'=>'title'])}}
    <small style="color:red" id="title_alert"></small>
    </div>
    </div>
    <div class="form-group row">
    {{Form::label('content','Description du Programme *',['class'=>'col-lg-3 col-form-label form-control-label'])}}
    <div class="col-lg-9">
        
     <textarea name="content" id="content" class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
    <small style="color:red" id="content_alert"></small>
    </div>
    </div>
    
    
    <div class="form-group row">
    {{Form::label('status','Status du programme *',['class'=>'col-lg-3 col-form-label form-control-label '])}}
    <div class="col-lg-9">
    {{Form::select('status',[
		'1'=>'Actif',
        '0'=>'Désactivé',
    ],null,['id'=>'status','class'=>'form-control'])}}
    <small style="color:red" id="status_alert"></small>
    </div>
    </div>
    
    <div class="form-group row">
    {{Form::label('image','Image du programme',['class'=>'col-lg-3 col-form-label form-control-label '])}}
    <div class="col-lg-9">
    {{Form::file('image',['id'=>'programimage'])}}
    </div>
    </div>
    <div class="form-group row">
    <label class="col-lg-3 col-form-label form-control-label">

    </label>
    <div class="col-lg-9">
    <a onclick="window.history.back();" type="button" class="btn btn-secondary pull-left">
        Fermer
    </a>

    <input type="submit" class="btn btn-primary pull-right" value="Sauvegarder">
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>
    </div>
    
    {{Form::close()}}



      </div>
        
        
      </div>

@endsection