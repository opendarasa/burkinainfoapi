<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('img/users/'.Auth::user()->profile_pic)}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form" style="display:none;">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        @if(Auth::user()->roleid==1)
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Utilisateurs</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('users.index')}}"><i class="fa fa-list"></i> Liste des Utilisateurs</a></li>
            <li><a href="{{route('users.create')}}"><i class="fa fa-plus"></i> Ajouter Un Utilisateur</a></li>
          </ul>
        </li>
        @endif
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clock-o"></i>
            <span>Programmes</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right"></span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('programs.index')}}"><i class="fa fa-list"></i> Liste des Programmes</a></li>
            <li><a href="{{route('programs.create')}}"><i class="fa fa-plus"></i> Ajouter Un Programme</a></li>
           
          </ul>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-play"></i> <span>Émissions</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">coming soon</small>
            </span>
          </a>
        </li>
       
        
        
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>