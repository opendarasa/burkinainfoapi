<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js" integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js" integrity="sha256-S1J4GVHHDMiirir9qsXWc8ZWw74PHHafpsHp5PXtjTs=" crossorigin="anonymous"></script>


<script type="text/javascript">
  

  function readURL() {
    var input=$("#programimage");
    console.log(input[0].files.size);
  
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#previewImage').attr('src', e.target.result);
    }
    console.log(e.target.result);
    
    reader.readAsDataURL(input[0].files);
  
}

	function submitProgram(){
		
      
      var title=$("#title").val();
      var content=$("#content").val();
      

      if(title==''){
        $("#title").focus();
        $("#title_alert").html("le champ titre est vide");
        formCheck=false;
        return formCheck;

      }else{
        $("#title_alert").html("");
        formCheck=true;
      }
      if(content==''){
        $("#content").focus();
        $("#content_alert").html("Le champ description est vide");
        formCheck=false;
        return formCheck;
      }else{
        $("#content_alert").html("");
         formCheck=true;
      }

      
     return false;
	}


 
  
	window.onload=function(){

    

$('#example2').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
		   

     $("#edit_user_form").submit(function(){
      //alert();
      var name=$("#name").val();
      var email=$("#email").val();
      //var type=$("#type").val();
      var username=$("#username").val();
      var status=$("#status").val();
      var role=$("#role").val();

      if(name==''){
        $("#name").focus();
        $("#name_alert").html("le champ nom est vide");
        formCheck=false;
        return formCheck;

      }else{
        $("#name_alert").html("");
        formCheck=true;
      }
      if(email==''){
        $("#email").focus();
        $("#email_alert").html("Le champ E-mail est vide");
        formCheck=false;
        return formCheck;
      }else{
        $("#email_alert").html("");
         formCheck=true;
      }
      if(username==''){
        $("#username").focus();
        $("#username_alert").html("Le champ Pseudonyme est vide");
        formCheck=false;
        return formCheck;
      }else{
        $("#username_alert").html("");
         formCheck=true;
      }
      if(status==''){
        $("#status").focus();
        $("#status_alert").html("Veuillez choisir le status actue de l'utilisateur ");
        formCheck=false;
        return formCheck;
      }else{
        $("#status_alert").html("");
         formCheck=true;
      }

      if(role==''){
        $("#role").focus();
        $("#role_alert").html("Veuillez choisir le role actuel de l'utilisateur ");
        formCheck=false;
        return formCheck;
      }else{
        $("#role_alert").html("");
         formCheck=true;
      }
     

      return formCheck;
     });

       
      
		   
           const app = new Vue({
        el:"#app",
        data:{
            programs:{},
            url:"{{url('programs/')}}",
            token:"{{trans('app.token')}}"
            
        },
        mounted(){
            this.getPrograms();
            //this.listen();
        },
        methods:{
            getPrograms(){
                axios.get(`/api/all-programs/`,{ headers: { Authorization:this.token} })
                     .then((response)=>{
                        this.programs=response.data.programs;
                        //this.answers=response.data.responses;
                        console.log(response);
                     })
                     .catch(function(error){
                        console.log(error)
                     });
            },
           orderBy: function(sorKey) {
            this.sortKey = sorKey
            this.sortSettings[sorKey] = !this.sortSettings[sorKey]
            this.desc = this.sortSettings[sorKey]
            
        },
            /*listen(){
                Echo.channel('question.'+this.room_id)
                    .listen('.new.question',(response) => {
                        this.questions=response.questions;
                        this.total+=1;
                        //console.log(question);
                    });

                Echo.channel('response.'+this.room_id)
                    .listen('.response.toquestion',(response)=>{

                        this.answers=response.response;
                        
                    });
                Echo.channel('remove.'+this.room_id)
                    .listen('.remove.question',(response)=>{
                        this.questions=response.questions;
                        this.total-=1;
                        console.log(response.questions);

                    });
                    Echo.channel('display.'+this.room_id)
                    .listen('.display',(response)=>{
                        this.questions=response.questions;
                        this.total+=1;
                        console.log(response.questions);

                    });
            },*/

            viewProgram(slug){
               window.location.href=this.url+'/'+slug;
            }
        }
    })

	}
 
</script>
