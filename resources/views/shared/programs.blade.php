<div class="col-sm-6 pull-right">
  @include('shared.search')
</div>

<div class="col-sm-6 pull-left">
  <a href="{{route('programs.create')}}" class="badge badge-success">
    <i class="fa fa-plus">

    </i>
  </a>
</div>

<table id="example2" class="table table-bordered table-striped">
    <thead>
                <tr>
                  <th>Image</th>
                  <th>Titre</th>
                  <th>Description</th>
                  <th>Date de Publication</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody v-for="(program,index) in programs">
                <tr>
                  <td>
                      <img v-bind:src="`${program.image}`" class="user-image" style="width:100px;height:100px;">
                  </td>
                  <td>@{{program.title}}
                  </td>
                  <td v-html="program.content">@{{program.content}}</td>
                  <td>@{{program.date}}</td>
                  <td >
                        Actif

                        
                  </td>
                  
                  <td>
                    <a @click="viewProgram(program.slug)" class="badge badge-primary">
                   <i class="fa fa-pencil">
                    modifier
                   </i>
                    </a>
                     <a id="deletebtn_''" @click="deleteFunction(program.slug)">
                       <i class="fa fa-trash"></i>
                    </a>&nbsp;
                    <form style="display:none;" v-bind:id="'deleteform_'+program.slug" v-bind:action="'programs/'+program.slug" method="POST">
                     @method('DELETE')
                       @csrf  
                      </form>
                  </td>
                </tr>
                
                </tbody>
                <tfoot>
               <tr>
                  <th>Image</th>
                  <th>Titre</th>
                  <th>Description</th>
                  <th>Date de Publication</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
    
