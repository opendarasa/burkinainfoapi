<div class="col-sm-6 pull-right">
  @include('shared.search')
</div>
@if(Auth::user()->roleid==1)
<div class="col-sm-6 pull-left">
  <a href="{{route('users.create')}}" class="badge badge-success">
    <i class="fa fa-plus">

    </i>
  </a>
</div>
@endif
<table id="example2" class="table table-bordered table-striped">
    <thead>
                <tr>
                  <th>Image</th>
                  <th>Nom complet</th>
                  <th>Role</th>
                  <th>Date de creation</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody >
                  @foreach($users as $user)
                <tr>
                  <td>
                      <img src="{{asset('img/users/'.$user->profile_pic)}}" class="user-image" style="width:100px;height:100px;">
                  </td>
                  <td>{{$user->name}}
                  </td>
                  <td >{{trans('app.role.'.$user->roleid)}}</td>
                  <td>{{$user->created_at}}</td>
                  <td>{{trans('app.status.'.$user->status)}}</td>
                  <td>
                    <a href="{{route('users.edit',$user->username)}}" class="badge badge-primary">
                   <i class="fa fa-pencil">
                    modifier
                   </i>
                    </a>
                    <a id="deletebtn" onclick="deleteFunction('{{$user->id}}')">
                       <i  class="fa fa-trash"></i>
                    </a>&nbsp;
                    <form style="display:none;" id="deleteform_{{$user->id}}" action="{{route('users.destroy',[$user->id])}}" method="POST">
                     @method('DELETE')
                       @csrf  
                      </form>
                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
               <tr>
                  <th>Image</th>
                  <th>Nom complet</th>
                  <th>Role</th>
                  <th>Date de creation</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
    
