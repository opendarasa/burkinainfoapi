<!-- jQuery 3 -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<!-- page script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.min.js" integrity="sha256-chlNFSVx3TdcQ2Xlw7SvnbLAavAQLO0Y/LBiWX04viY=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js" integrity="sha256-S1J4GVHHDMiirir9qsXWc8ZWw74PHHafpsHp5PXtjTs=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha256-KsRuvuRtUVvobe66OFtOQfjP8WA2SzYsmm4VPfMnxms=" crossorigin="anonymous"></script>
<script>
  function deleteFunction(id) {
    swal({
  title: "Ëtes-vous sûr de vouloir suprimer?",
  text: "Une fois suprimées ces données ne seront plus accessibles",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    
    swal("Supprimé", {
      icon: "success",
    });
    $("#deleteform_"+id).submit();

  } else {
    swal("Vos données sont intactent!");
  }
});
}
  window.onload=function(){

    

$('#example2').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })

 
           const app = new Vue({
        el:"#app",
        data:{
            programs:{},
            url:"{{url('programs/')}}",
            token:"{{trans('app.token')}}"
            
        },
        mounted(){
            this.getPrograms();
            //this.listen();
        },
        methods:{
            getPrograms(){
                axios.get(`/api/all-programs/`,{ headers: { Authorization:this.token} })
                     .then((response)=>{
                        this.programs=response.data.programs;
                        //this.answers=response.data.responses;
                        console.log(response);
                     })
                     .catch(function(error){
                        console.log(error)
                     });
            },
           orderBy: function(sorKey) {
            this.sortKey = sorKey
            this.sortSettings[sorKey] = !this.sortSettings[sorKey]
            this.desc = this.sortSettings[sorKey]
            
        },
            /*listen(){
                Echo.channel('question.'+this.room_id)
                    .listen('.new.question',(response) => {
                        this.questions=response.questions;
                        this.total+=1;
                        //console.log(question);
                    });

                Echo.channel('response.'+this.room_id)
                    .listen('.response.toquestion',(response)=>{

                        this.answers=response.response;
                        
                    });
                Echo.channel('remove.'+this.room_id)
                    .listen('.remove.question',(response)=>{
                        this.questions=response.questions;
                        this.total-=1;
                        console.log(response.questions);

                    });
                    Echo.channel('display.'+this.room_id)
                    .listen('.display',(response)=>{
                        this.questions=response.questions;
                        this.total+=1;
                        console.log(response.questions);

                    });
            },*/

            viewProgram(slug){
               window.location.href=this.url+'/'+slug;
            },
            deleteFunction(id) {
              console.log(id);
    swal({
  title: "Ëtes-vous sûr de vouloir suprimer?",
  text: "Une fois suprimées ces données ne seront plus accessibles",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    
    swal("Supprimé", {
      icon: "success",
    });
    $("#deleteform_"+id).submit();

  } else {
    swal("Vos données sont intactent!");
  }
});
}
        }
    })
}
function SearchWord() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("searchInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("example2");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    var td2 = tr[i].getElementsByTagName("td")[1];
    var td3 = tr[i].getElementsByTagName("td")[2];
    var td4 = tr[i].getElementsByTagName("td")[3];
    if (td||td2||td3||td4) {
      txtValue = td.textContent || td.innerText||td2.textContent||td2.innerText||td3.textContent||td3.innerText||td4.textContent||td4.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}

</script>