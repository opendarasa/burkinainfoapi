<!DOCTYPE html>
<html>
@include('shared.head2')
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper" >

  @include('shared.header')
  @include('shared.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="app">
   @include('shared.breadcrumb')

    <!-- Main content -->
    <section class="content">
      @yield('content')

    </section>
    <!-- /.content -->
  </div>
  @include('shared.footer')

  @include('shared.rightsidebar')
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@include('shared.js2')
</body>
</html>
