

## About Burkina Info API

This documents is an introduction to the API endpoints of  of a news media organization namely "Burkina Info” mobile application. This document and its content are strictly confidential . Any attempt to copy this document and/or its content without prior consent of the owners(Burkina Info, Dr Moise Convolbo, Jean Ouedraogo) is liable to legal poursuit.


## Installation
Run the following command to install the API  on your local  computer.

```
// clone the project

 $ git clone https://gitlab.com/opendarasa/burkinainfoapi.git

// Navigate to the directory of the proeject

 $ cd burkinainfoapi

 // use composer to install the project

 $ composer install

 //copy the .env.example  file to .env file and setup the database configuration then migrate by running the command below

 $ cp .env.example .env

 // Open the .env and setup the database credentials 

DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=your-database-name
DB_USERNAME=your-database-username
DB_PASSWORD=your-database-password

 $ php artisan migrate

 // Run the database seeder

 $ php artisan seed:db

```

 and that's it ! your API is ready 
## API Endpoints and testing

Please click [here](https://docs.google.com/document/d/1XEb6CQKdmaBE8WrJ7l8cZpCTmhMHwCIdTikf2S6ltpc/edit?usp=sharing) to get the API documentation where endpoints are specified.

Install [Postman](https://www.getpostman.com/downloads/) to start testing your API.


## API update 

Just run the commands below to update your projuect

```
$ git pull origin master
```

## Security Vulnerabilities

If you discover a security vulnerability within this project, please send an e-mail to Jean Ouedraogo via [ouedmenga@gmail.com](mailto:ouedmenga@gmail.com). All security vulnerabilities will be promptly addressed.

