<?php 
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Image;
use Auth;
use App\Emission;
use App\Program;
use Carbon\Carbon;
interface RepositoryInterface
{
    public function all();
    public function allUsersById();
    public function allUsers();

    public function create(array $data);

    public function update(array $data, $slug);
    public function updateUser(array $data, $username);

    public function delete($id);

    public function show($id);
    public function showUser($username);

    public function validateEmission(Request $request);
    public function validateProgram(Request $request);
    public function validateUpdatedEmission(Request $request,$slug);
    public function validateUpdatedProgram(Request $request,$slug);

    public function renderHTML($description);

    public function saveWorkFile($file);
    public function saveBlogFile($file);

    public function allById();

    public function validateUser(Request $request);
    public function getUserData(Request $request);
    public function ValidateUpdateUser(Request $request,$user);
   public function getUpdatedUserData(Request $request,$user);
   public function saveUserImage($file);
   //public function set_default_meta($ogimage,$ogtitle,$ogdescription);


}

class Repository implements RepositoryInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        return $this->model->where('status',1)->orderBy('created_at','asc')->get();
    }

    // create a new record in the database
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $slug){
         $record=$this->model->where('slug',$slug)->first();

         return $record->update($data);
    }

    // update record in the database
    public function updateUser(array $data, $username)
    {
        $record = $this->model->where('username',$username)->first();
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($slug)
    {
        return $this->model->where('slug',$slug)->first();
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function allById(){
        return $this->model->where('status',1)->orderBy('created_at','desc')->get();
    }
    public function allUsersById()
    {
        return $this->model->where('hidden',0)->orderBy('created_at','desc')->get();
    }

    public function allUsers()
    {
        return $this->model->where('hidden',0)->orderBy('created_at','asc')->get();
    }


    public function showUser($username)
    {
        return $this->model->where('username',$username)->first();
    }

    public function validateEmission(Request $request)
    {
        //var_dump($request->all());
        //exit();
       $validator=Validator::make($request->all(),[
         'title'=>'required|string|max:100',
         'video_url'=>'required|string',
         'content'=>'required|string',
         'image'=>'required|image',
         //'rank'=>'integer',
         'date'=>'string',

        ]);

        if($validator->fails())
        {
            return  [ 
                     'status'=>false,
                     'data'=>$validator,

                   ];
        }
        else{
            $data['title']=$request->input('title');
            $data['video_url']=$request->input('video_url');
            $data['content']=$this->renderHTML($request->input('content'));
            $data['image']=$this->saveBlogFile($request->file('image'));
            $data['rank']=$request->input('rank');
            //$data['tags']=($request->has('tags'))?$request->input('tags'):'';
            $data['user_id']=Auth::user()->id;
            $data['slug']=str_slug($request->input('title'),'-');
            
            return [ 
                     'status'=>true,
                     'data'=>$data,

                   ];
        }
    }

    public function validateUpdatedEmission(Request $request,$slug)
    {
        //var_dump($request->all());
        //exit();
       $validator=Validator::make($request->all(),[
         'title'=>'required|string|max:100',
         'video_url'=>'required|string',
         //'content'=>'required|string',
         'image'=>'sometimes',
         //'rank'=>'integer',
         'date'=>'string',

        ]);

        if($validator->fails())
        {
            return  [ 
                     'status'=>false,
                     'data'=>$validator,

                   ];
        }
        else{
            $data['title']=$request->input('title');
            $data['video_url']=$request->input('video_url');
            //$data['content']=$this->renderHTML($request->input('content'));
            if ($request->file('image')) {
            $data['image']=$this->saveBlogFile($request->file('image'));
            }
            else{
                $blog=Emission::where('slug',$slug)->first();
                $data['image']=$blog->image;
            }
            
            //$data['cat_id']=$request->input('cat_id');
            $data['tags']=($request->has('tags'))?$request->input('tags'):'';
            $data['user_id']=$request->input('user_id');
            //$data['slug']=str_slug($request->input('title'),'-');
          
            return [ 
                     'status'=>true,
                     'data'=>$data,

                   ];
        }
    }


   public function validateProgram(Request $request)
    {
        //var_dump($request->all());
        //exit();
       $validator=Validator::make($request->all(),[
         'title'=>'required|string|max:100',
         //'video_url'=>'required|string',
         'content'=>'required|string',
         'image'=>'required|image',
         //'cat_id'=>'integer',
         //'date'=>'string',
         //'tags'=>'sometimes|string',

        ]);

        if($validator->fails())
        {
            return  [ 
                     'status'=>false,
                     'data'=>$validator,

                   ];
        }
        else{
            $data['title']=$request->input('title');
            //$data['video_url']=$request->input('video_url');
            $data['content']=$this->renderHTML($request->input('content'));
            
            $tmp=$this->saveWorkFile($request->file('image'));
            $data['image']=url('img/program/').'/'.$tmp;
            $data['date']=Carbon::now();
            //$data['client']=$request->input('client');
            //$data['tags']=($request->has('tags'))?$request->input('tags'):'';
            $data['user_id']=Auth::user()->id;
            $data['slug']=str_slug($request->input('title'),'-');
          
            return [ 
                     'status'=>true,
                     'data'=>$data,

                   ];
        }
    }
    public function validateUpdatedProgram(Request $request,$slug)
    {
        //var_dump($request->all());
        //exit();
       $validator=Validator::make($request->all(),[
         'title'=>'required|string|max:100',
         //'video_url'=>'required|string',
         'content'=>'required|string',
         'image'=>'sometimes',
         'date'=>'string',
         //'client'=>'required|string',
         //'tags'=>'sometimes|string',

        ]);

        if($validator->fails())
        {
            return  [ 
                     'status'=>false,
                     'data'=>$validator,

                   ];
        }
        else{
            $data['title']=$request->input('title');
            //$data['video_url']=$request->input('video_url');
            $data['content']=$this->renderHTML($request->input('content'));
            if ($request->file('image')) {
                
                $tmp=$this->saveWorkFile($request->file('image'));
                $data['image']=url('img/program/').'/'.$tmp;
            }
            else{

                $work=Program::where('slug',$slug)->first();
                $data['image']=$work->image;
            }
            $work=Program::where('slug',$slug)->first();
            //$data['cat_id']=$request->input('cat_id');
            $data['date']=$request->input('date');
            //$data['date']=($request->has('tags'))?$request->input('tags'):'';
            $data['user_id']=$work->user_id;
            //$data['slug']=str_slug($request->input('title'),'-');
          
            return [ 
                     'status'=>true,
                     'data'=>$data,

                   ];
        }
    }
    public function renderHTML($description)
    {

       $dom=new \DOMDocument();
        libxml_use_internal_errors(false);
        @$dom->loadHtml( mb_convert_encoding($description, 'HTML-ENTITIES', "UTF-8"));

        $images=$dom->getElementsByTagName('img');
        
        foreach($images as $img)
        {
            $src=$img->getAttribute('src');
            
            if(preg_match('/data:image/',$src))
            {
                
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimetype = $groups['mime'];
                
                // Generating a random filename
                $filename = uniqid();
                $filepath = "/images/$filename.$mimetype";
                $image=Image::make($src)->encode($mimetype,100)->save(public_path($filepath));
                
                $new_src=asset($filepath);
                $img->removeAttribute('src');
                
                $img->setAttribute('src',$new_src);
                
            }
            
        }
               $description=$dom->saveHTML();

               return $description;  
    }

    public function saveWorkFile($file){
        $path=public_path('img/program');
        $filename=uniqid();
        //Image::make($file->getRealPath())->resize(400,300)->save($path.'/'.$filename);
        //Image::make($file->getRealPath())->resize(700,933)->save($path.'/'.$filename.'_full');
         $file->move($path,$filename);
        return $filename;
    }

    public function saveBlogFile($file){
        $path=public_path('img/emission');
        $filename=uniqid();
        Image::make($file->getRealPath())->resize(400,300)->save($path.'/'.$filename);
        Image::make($file->getRealPath())->resize(700,933)->save($path.'/'.$filename.'_full');
        return $filename;
    }

    public function validateUser(Request $request)
    {
         $validation=Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            //'profile_pic'=>'sometimes',
        ]);
         if($validation->fails())
         {
            return [
                    'status'=>false,
                    'data'=>$validation,
                  ];
         }
         else{
            $data=$this->getUserData($request);
               
            return [
                     'status'=>true,
                     'data'=>$data,
                   ];
         }
    }

    public function getUserData(Request $request)
    {
        $data['name']=$request->input('name');
               $data['email']=$request->input('email');
               $data['password']=Hash::make($request->input('password'));
               $profile_pic='avatar.png';
               if($request->file('image'))
               {
                $tmp=$request->file('image');
                 $profile_pic=$this->saveUserImage($tmp);
               }
               $data['profile_pic']=$profile_pic;
               $data['username']=uniqid();
               //$data['skills']=$request->input('skills');
               
            return $data;
    }

    public function ValidateUpdateUser(Request $request,$user)
    {
        if ($user->ema) {
            # code...
        }
        
         $validation=Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'profile_pic'=>'sometimes|image|max:10240|',
            //'email'=>'required|email|unique:users',
            'roleid'=>'required',
            'status'=>'required',

        ]);
         if($validation->fails())
         {
            return [
                    'status'=>false,
                    'data'=>$validation,
                  ];
         }
         else{


          $profileimage='';
          $profilecover='';
         if($request->file('profile_pic'))
         {
            $profilepic=$request->file('profile_pic');
            $profileimage=$this->saveUserImage($profilepic);
         }
         
            $data=$this->getUpdatedUserData($request,$user);
            if($profileimage!=''){
                $data['profile_pic']=$profileimage;
            }

            return [
                     'status'=>true,
                     'data'=>$data,
                   ];
         }
            
            

    }

     public function getUpdatedUserData(Request $request,$user)
    {
               $data['name']=$request->input('name');
               $data['email']=$request->input('email');
               $data['status']=$request->input('status');
               $data['roleid']=$request->input('roleid');
               //$data['image']='avatar.png';
               $data['username']=$user->username;
               

               
            return $data;
    }

    public function saveUserImage($file)
    {
        $path=public_path('img/users');
        $filename=uniqid();
        Image::make($file->getRealPath())->resize(150,150)->save($path.'/'.$filename);

        return $filename;
       
    }
    /*public function saveUserCover($file)
    {
        $path=public_path('img/users');
        $filename=uniqid();
        Image::make($file->getRealPath())->resize(1500,1000)->save($path.'/'.$filename);

        return $filename;
       
    }*/

    /**
     * Set the meta image , description and tagline
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /*public function set_default_meta($ogimage,$ogtitle,$ogdescription)
    {
        return [
             'ogimage'=>$ogimage,
             'ogtitle'=>$ogtitle,
             'ogdescription'=>$ogdescription,
        
        ];
    }*/


}