<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    //
    protected $fillable=['title','video_url','user_id','image','slug','date','content'];
}
