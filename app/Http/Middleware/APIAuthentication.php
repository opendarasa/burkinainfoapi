<?php

namespace App\Http\Middleware;

use Closure;
use Request;
class APIAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //$token=$request->headers('Authorization');

        $token=Request::header('Authorization');
        $match=trans('app.token');

       if($token!=$match)
       {
         return 
         response()->json([
            'status'=>'error',
            'message'=>'Token does not exist',
         ]);
       }
        return $next($request);
    }
}
