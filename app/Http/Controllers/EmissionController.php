<?php

namespace App\Http\Controllers;
use App\Repositories\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Emission;
use Cache;
class EmissionController extends Controller
{


    // constructor 

    protected $model;

    public function __construct(Emission $model)
    {
        $this->model=new Repository($model);

        

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
    {
        return view('emissions.index');
    }

   
    public function ApiGetAll()
    {

            //Cache::forget('emissions');

            if(Cache::has('emissions'))
            {

                $data['emissions']=Cache::get('emissions');

                return response()->json($data);
                
            }else
            {
                 $data['emissions']=DB::table('emissions')->join('users',function($join){
            $join->on('users.id','=','emissions.user_id')->where('emissions.status',1);
        })->orderBy('emissions.date','desc')->select('users.name as author_name','users.profile_pic as author_picture','emissions.title','emissions.content','emissions.video_url','emissions.image','emissions.date','emissions.slug')->get();

        if(count($data['emissions'])>0)
        {
            foreach ($data['emissions'] as $emission) {
                
        $emission->image=url('/').'/img/emission/'.$emission->image;
        
        $emission->author_picture=url('/').'/img/users/'.$emission->author_picture;

            }

            Cache::put('emissions',$data['emissions']);

            return response()->json($data);
        }else
        {
            $error['status']='0';
            $error['message']='emissions table is empty';

            return response()->json($error);
        }
            }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('emissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->ajax())
        {
           $result=$this->model->validateEmission($request);

        if($result['status']==true)
        {
            $this->model->create($result['data']);

            $response['status']='success';
            $response['message']='emissions created';
            $response['slug']=$result['slug'];

            echo json_encode($response);
        }else{

             $response['status']='success';
            $response['message']='emissions created';
            echo json_encode($response);

        } 
    }else{
        echo json_encode(['status'=>'error','message'=>'post empty']);
    }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return view('emissions.show')->with(compact('slug'));

    }

    /**
     * get the specified resource via API call.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function ApiShow($slug)
    {
        //
       $emission=array();
        if (Cache::has('emission_'.$slug)) {
           $emission=Cache::get('emission_'.$slug);
           return response()->json($emission);
        }else{
             $emission=$this->model->show($slug);
        

        if(count($emission)>0)
        {
            $author=User::find($emission->user_id);

        $emission->image=url('/').'/img/emission/'.$emission->image;
        $emission->author_name=(count($author)>0)?$author->name:'Unknown';
        $emission->author_picture=(count($author)>0)?url('/').'/img/users/'.$author->profile_pic:url('/').'/img/users/avatar.png';
          unset($emission->id);
          unset($emission->user_id);
          unset($emission->created_at);
          unset($emission->updated_at);
          unset($emission->rank);
          unset($emission->status);
          Cache::put('emission_'.$slug,$emission);
            return response()->json($emission);
        }else{

            $error['status']='404';
            $error['message']='emission not  found';

            return response()->json($error); 
        }
        }
       

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $emission=$this->model->show($slug);

        return view('emissions.edit')->with(compact('emission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->ajax())
        {
            $result=validateUpdatedEmission($request,$slug);

        if($result['status']==true)
        {
            $this->model->update($result['data'],$slug);

             $response['status']='success';
            $response['message']='emissions created';
            $response['slug']=$slug;

            echo json_encode($response);

        }else{
             $response['status']='error';
            $response['message']='emissions not updated';
            

            echo json_encode($response);
         }

        }else{
            echo json_encode(['status'=>'error','message'=>'post empty']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    // ajax delete

     public function ajaxDelete(Request $request)
    {
        if($request->ajax())
        {
            $this->model->delete($request->id);
            echo json_encode(['status'=>'success','message'=>'deleted successfully']);
        }else{
            echo json_encode(['status'=>'error','message'=>'post empty']);

        }
    }
}
