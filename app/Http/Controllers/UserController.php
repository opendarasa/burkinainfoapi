<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Repositories\Repository;
class UserController extends Controller
{

     protected $model;
     public function __construct(User $model)
     {
         $this->model=new Repository($model);
     }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=$this->model->all();

        return view('users.index')->with(compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        $user=$this->model->showUser($username);

        return view('users.edit')->with(compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($username)
    {
        return $this->show($username);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username)
    {
        //
        $user=$this->model->showUser($username);
        if(count($user)>0)
        {

            $data=$this->model->ValidateUpdateUser($request,$user);

        if($data['status']==true)
        {
            $this->model->updateUser($data['data'],$username);

            return response()->json([
                'status'=>'success',
                'message'=>'user updated',
            ]);
            Session::flash('status','Modification réussi');
            return redirect()->back();

        }else{
            return redirect()->back()->withErrors($data['data'])->withInput();
        }

        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->delete();

        return redirect()->back();
    }
}
