<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\Repository;
use App\User;
use App\Program;
use  Cache;
use Session;
class ProgramController extends Controller
{
     // constructor 

    protected $model;

    public function __construct(Program $model)
    {
        $this->model=new Repository($model);

        $this->middleware('auth', ['except' => array('ApiShow', 'ApiGetAll')]);

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function index()
    {
        return view('programs.index');
    }

   
    public function ApiGetAll()
    {
        Cache::forget('programs');

        if (Cache::has('programs')) {
            $programs=Cache::get('programs');

            return response()->json($programs);
        }
        $data['programs']=DB::table('programs')->join('users',function($join){
            $join->on('users.id','=','programs.user_id')->where('programs.status',1);
        })->orderBy('programs.date','asc')->select('users.name as author_name','users.profile_pic as author_picture','programs.title','programs.content','programs.video_url','programs.image','programs.date','programs.slug')->get();

        if(count($data['programs'])>0)
        {
            foreach ($data['programs'] as $program) {
                
        //$program->image=url('/').'/img/program/'.$program->image;
        
        $program->author_picture=url('/').'/img/users/'.$program->author_picture;

            }
            Cache::put('programs',$data);
            return response()->json($data);
        }else
        {
            $error['status']='0';
            $error['message']='programs table is empty';

            return response()->json($error);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('programs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
           $result=$this->model->validateProgram($request);

        if($result['status']==true)
        {
            $this->model->create($result['data']);

            $response['status']='success';
            $response['message']='programs created';
            //$response['slug']=$result['slug'];
            Cache::forget('programs');
            Session::flash('status','Creation Réussi');
            return redirect()->back();
        }else{

             $response['status']='success';
            $response['message']='programs created';

            return redirect()->back()->withErrors($result['data'])->withInput();

        } 
    
        
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return $this->edit($slug);

    }

    /**
     * get the specified resource via API call.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function ApiShow($slug)
    {
        
        $program=array();
        if (Cache::has('program_'.$slug)) {
            $program=Cache::get('program_'.$slug);

            return response()->json($program);
        }else{
             //
        $program=Program::where('slug',$slug)->first();
         
        if(count($program)>0)
        {
            $author=User::find($program->user_id);

        //$program->image=url('/').'/img/program/'.$program->image;
        $program->author_name=(count($author)>0)?$author->name:'Unknown';
        $program->author_picture=(count($author)>0)?url('/').'/img/users/'.$author->profile_pic:url('/').'/img/users/avatar.png';
          unset($program->id);
          unset($program->user_id);
          unset($program->created_at);
          unset($program->updated_at);
          unset($program->rank);
          unset($program->status);
          Cache::put('program_'.$slug,$program);
            return response()->json($program);
        }else{

            $error['status']='404';
            $error['message']='program not  found';

            return response()->json($error); 
        }

        }
       

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $program=$this->model->show($slug);
        $users=User::pluck('name','id');

        return view('programs.edit')->with(compact('program','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$slug)
    {
        
            $result=$this->model->validateUpdatedProgram($request,$slug);

        if($result['status']==true)
        {
            $this->model->update($result['data'],$slug);

             $response['status']='success';
            $response['message']='programs created';
            $response['slug']=$slug;
            Cache::forget('program_'.$slug);
             Session::flash('status','Modification réussi');
            return redirect()->back();

        }else{
             $response['status']='error';
            $response['message']='programs not updated';
            
            return redirect()->back()->withErrors($result['data'])->withInput();
            //echo json_encode($response);
         }

        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        Program::where('slug',$slug)->delete();
        Cache::forget('program_'.$slug);
        return redirect()->back();
    }

    // ajax delete

     public function ajaxDelete(Request $request)
    {
        if($request->ajax())
        {
            $this->model->delete($request->id);
            echo json_encode(['status'=>'success','message'=>'deleted successfully']);
        }else{
            echo json_encode(['status'=>'error','message'=>'post empty']);

        }
    }
}
