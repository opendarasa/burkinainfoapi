<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class ApiController extends Controller
{
    // Variable Initialization

    protected $client_id;
    protected $client_secret;

    protected $callback_uri;

    //protected $client;

    // contructor and global variable assignment

     public function __construct()
    {
    	$client=DB::table('oauth_clients')->whereNotNull('secret')->first();

    	if(count($client)>0)
    	{
    		$this->client_id=$client->id;
    		$this->client_secret=$client->secret;
    		$this->callback_uri=$client->redirect;
    	}else{

    		echo json_encode([
    			'status'=>'Error',
    			'message'=>'Client table empty or client user id does not exist',
    			'resolution'=>'Please run the command below in your project directory',
    			'command'=>'php artisan passport:cient',
    		]);

    		exit();
    	}
    }

   // Get authorization  with client credientials 
    public function grantAccessCode()
    {


    	$query = http_build_query([
        'client_id' => $this->client_id,
        'redirect_uri' => $this->callback_uri,
        'response_type' => 'code',
        'scope' => '*',
    ]);


    return redirect('/oauth/authorize?'.$query);
    }


    // get access token upon authorization
    public function callback(Request $request)
    {
    	 $http = new Client();
  		$token_url=url('oauth/token');
  		
    $response = $http->post($token_url, [
        'form_params' => [
            'grant_type' => 'password',
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'username'=>'my-username',
            'password'=>'my-password',
            'scope' =>'*',
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
    }


     public function generateToken()
     {

        
         return Hash::make('burkinaInfoToken#'.Carbon::now());
     }


}
