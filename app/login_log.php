<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class login_log extends Model
{
    //
    protected $fillable=['email','login_status','ip'];
}
